## TODO
- [x] make a static helper class to store small functions, like saving & retrieving maps into shared preferences  

- [ ] there's a minor bug in showing small resources bar: when it's updated from warning to not warning, it's not changing its color back to green   

- [x] give buildings in queue a smooth slide out transition   

- [x] upgrade buildings in queue

- [ ] consider using SQLite to query img files instead of like 200 lines conditioning in Building class    

- [x] swipe to delete items

- [ ] show inner buildings

- [x] refresh outer building list with Document requested by other actions 

- [x] crawl buildings db: build time, requirements, etc.