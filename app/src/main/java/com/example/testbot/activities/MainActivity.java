package com.example.testbot.activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.testbot.Constants.FragmentFlags;
import com.example.testbot.Constants.RequestCode;
import com.example.testbot.Constants.ResultCode;
import com.example.testbot.fragments.BuildingsFragment;
import com.example.testbot.fragments.InnerBuildingsFragment;
import com.example.testbot.fragments.OuterBuildingsFragment;
import com.example.testbot.R;
import com.example.testbot.adapters.PageAdapter;
import com.example.testbot.assets.Building;
import com.example.testbot.assets.BuildingQueue;
import com.example.testbot.assets.UpgradingBuilding;
import com.example.testbot.assets.User;
import com.example.testbot.assets.VillageResources;
import com.example.testbot.viewmodels.MainViewModel;
import com.example.testbot.views.MainActivityView;
import com.example.testbot.views.ResourcesView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static androidx.fragment.app.FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;
import static com.example.testbot.Constants.FragmentIndex.INNER_BUILDINGS;
import static com.example.testbot.Constants.FragmentIndex.OUTER_BUILDINGS;
import static com.example.testbot.Constants.TIMEOUT_LIMIT;

public class MainActivity extends AppCompatActivity implements  BuildingsFragment
                                                                    .OnFragmentInteractionListener {

    private MainViewModel mViewModel;
    private ResourcesView mResourcesView;
    private SwipeRefreshLayout mSrBuildings;
    private Timer mTimer = new Timer();
    private Snackbar mSbTimedOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainActivityView.initToolbar(this);
        MainActivityView.initIcons(this);
        initFragmentPager();
        initResourcesView();
        initLoginCredentials();
        initSwipeRefresh();
        mSrBuildings.setRefreshing(true);

        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        mViewModel.updateBuildingList();
        mViewModel.initTimer();

        mViewModel.outerBuildings().observe(this, new Observer<ArrayList<Building>>() {
            @Override
            public void onChanged(@Nullable ArrayList<Building> buildings) {
                assert buildings != null;

                // Pass new building list to fragment to display
                OuterBuildingsFragment fragment =
                    (OuterBuildingsFragment) getSupportFragmentManager()
                                                .getFragments()
                                                .get(OUTER_BUILDINGS);
                fragment.updateBuildingList(buildings);

                // Disable refreshing icon, disable timeout counter
                mSrBuildings.setRefreshing(false);
                mSbTimedOut.dismiss();
                try {
                    mTimer.cancel();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mViewModel.innerBuildings().observe(this, new Observer<ArrayList<Building>>() {
            @Override
            public void onChanged(ArrayList<Building> buildings) {
//                assert buildings != null;

                // Pass new building list to fragment to display
                InnerBuildingsFragment fragment =
                    (InnerBuildingsFragment) getSupportFragmentManager()
                                                .getFragments()
                                                .get(INNER_BUILDINGS);

                fragment.updateBuildingList(buildings);
            }
        });


        mViewModel.villageResources().observeForever(new Observer<VillageResources>() {
            @Override
            public void onChanged(VillageResources villageResources) {
                updateResourcesView(villageResources);
            }
        });


        mViewModel.newBuildings().observeForever(new Observer<ArrayList<String>>() {
            @Override
            public void onChanged(ArrayList<String> strings) {

                mSrBuildings.setRefreshing(false);
                for (String str : strings) {
                    Log.i("debug", str);
                    // TODO: remove this log
                }
            }
        });
    }


    /**
     * Grab and process data spitted out from child activities.
     * @param requestCode Request code of the activity triggering this method. See {@link RequestCode}
     * @param resultCode  Result code, indicating everything's working normally or not. See {@link ResultCode}
     * @param result      Intent to get the result from.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent result) {

        super.onActivityResult(requestCode, resultCode, result);
        if ((requestCode == RequestCode.BUILDING_QUEUE)
            && (resultCode == ResultCode.OK)
            && (result != null)) {

            BuildingQueue queue = result.getParcelableExtra("queuings");
            mViewModel.setUpgradingQueue(queue);
        }
    }


    /**
     * Show the upgrading queue popup if click the floating button
     * @param view Current view emitting this signal, just leave it to the xml file
     */
    public void showBuildingQueuePopup(View view) {
        ArrayList<UpgradingBuilding> upgradingBuildings = mViewModel.upgradingBuildings().getValue();

        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this);
        Intent i = new Intent(MainActivity.this, BuildingQueuePopupActivity.class);

        i.putExtra("upgradings", upgradingBuildings);
        i.putExtra("queuings", (Parcelable) mViewModel.getUpgradingQueue());

//        startActivity(i, options.toBundle());
        startActivityForResult(i, RequestCode.BUILDING_QUEUE, options.toBundle());
    }


    /**
     * Show the resource popup if the resource bar was clicked
     * @param view Current view emitting this signal, just leave it to the xml file
     */
    public void showResourcesPopup(View view) {
        if (ResourcesPopupActivity.isRunning) {
            //if is showing a popup, do nothing
        } else {
            //else show a new popup
            try {
                VillageResources village = mViewModel.villageResources().getValue();

                ActivityOptions options = ActivityOptions.makeCustomAnimation(this, R.anim.slide_in_top, R.anim.slide_out_top);
                Intent i = new Intent(MainActivity.this, ResourcesPopupActivity.class);
                i.putExtra("resource", village);
                startActivity(i, options.toBundle());


            } catch (NullPointerException e) {
                //if null aka somehow can't get village resources (connection error, bugs, etc.) then skip
                Log.i("debug", "Can't get village resources, @MainActivity/showResourcesPopup");
                ActivityOptions options = ActivityOptions.makeCustomAnimation(this, R.anim.slide_in_top, R.anim.slide_out_top);
                Intent i = new Intent(MainActivity.this, ResourcesPopupActivity.class);
                startActivity(i, options.toBundle());
            }
        }
    }


    /**
     * Update the resource bars if there resources variable changed
     * @param resources Newly updated resource
     */
    private void updateResourcesView(VillageResources resources) {
        mResourcesView.updateResources(resources);
    }


    private void initSwipeRefresh() {
        mSbTimedOut = Snackbar.make(findViewById(R.id.holder), "Server took too long to respond. Do you want to reload?", Snackbar.LENGTH_INDEFINITE);

        mSrBuildings = findViewById(R.id.srl_outer_buildings);
        mSrBuildings.setColorSchemeColors(getResources().getColor(R.color.dark_green, null));
        mSrBuildings.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshBuildingList();
            }
        });
    }


    /**
     * Initialize the View Pager along with its Fragments
     */
    private void initFragmentPager() {
        PageAdapter pageAdapter = new PageAdapter(getSupportFragmentManager(), BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        ViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(pageAdapter);
    }


    /**
     * Send a request to server to refresh building list. Schedule a timer to check if the reload process
     * took too long
     */
    private void refreshBuildingList() {
        mViewModel.updateBuildingList();

        mTimer = new Timer();
        //Prompt hard reload (login again) if timed out
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                MainActivityView.showTimedOutSnackbar(mSbTimedOut);

                mSbTimedOut.setAction("Reload", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewModel.login();
                        refreshBuildingList();
                    }
                });
            }
        }, TIMEOUT_LIMIT);
    }


    private void initLoginCredentials() {
        /*  remember to catch the case of logging in failed. In that case, the retrieved cookies will have
            only one entry.
        */
//      remove this when have an appropriate login activity

        User.setCredentials("farm101", "abc123", "https://tx3.travian.com/");
    }


    /**
     * Initialize the view handles resources-related GUI
     */
    private void initResourcesView() {
        mResourcesView = new ResourcesView(this);
    }


    @Override
    public void onButtonClicked(int index, FragmentFlags emitter) {
        // Pulse the floating button
        FloatingActionButton button = findViewById(R.id.fab_building_list);
        YoYo.with(Techniques.Pulse)
            .duration(400)
            .repeat(0)
            .playOn(button);

        // Add this building to upgrading queue
        ArrayList<Building> buildings = (emitter == FragmentFlags.INNER_BUILDINGS)?
                                            mViewModel.innerBuildings().getValue():
                                            mViewModel.outerBuildings().getValue();

        assert buildings != null;
        Building chosenOne = buildings.get(index);

        if (chosenOne.isBuildingSite) {
            mSrBuildings.setRefreshing(true);
            mViewModel.getAvailableNewBuildings(chosenOne);
            return;
        }

        BuildingQueue queue = mViewModel.getUpgradingQueue();
        queue.addBuilding(chosenOne.clone());
        mViewModel.setUpgradingQueue(queue);

    }
}
