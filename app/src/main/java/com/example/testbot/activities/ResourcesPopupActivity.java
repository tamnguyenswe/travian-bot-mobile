package com.example.testbot.activities;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.testbot.R;
import com.example.testbot.assets.VillageResources;
import com.example.testbot.views.PopupResourcesView;

import java.util.Timer;
import java.util.TimerTask;

public class ResourcesPopupActivity extends PopupActivity {
    private PopupResourcesView mResourcesView;
    private Timer mTimer;

    private VillageResources mVillage;
    private MutableLiveData<VillageResources> village = new MutableLiveData<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resources_popup);
        initIcons();

        mResourcesView = new PopupResourcesView(this);

        Intent intent = getIntent();
        mVillage = intent.getParcelableExtra("resource");

        village.observe(this, new Observer<VillageResources>() {
            @Override
            public void onChanged(@Nullable VillageResources villageResources) {
                assert villageResources != null;
                mResourcesView.timerUpdate(villageResources);
            }
        });


        showResources(mVillage);
        initTimer();
    }

    /**
     * Override to customize the transition
     * @param view The clicked view
     */
    @Override
    public void clickedOutside(View view) {
        super.clickedOutside(view);
        overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_top);
    }

    /**
     * Update resources GUI in bars and textviews
     * @param village new info about resources
     */
    private void showResources(VillageResources village) {
        mResourcesView.updateResources(village);
    }


    /**
     * Set icons to imageviews in this activity. Can't do with xml code since couldn't get <include>
     * tags to work with parameters
     */
    private void initIcons() {
        ImageView iconWarehouse = findViewById(R.id.warehouse).findViewById(R.id.icon);
        ImageView iconGranary = findViewById(R.id.granary).findViewById(R.id.icon);

        ImageView iconLumber = findViewById(R.id.lumber).findViewById(R.id.icon);
        ImageView iconClay = findViewById(R.id.clay).findViewById(R.id.icon);
        ImageView iconIron = findViewById(R.id.iron).findViewById(R.id.icon);
        ImageView iconCrop = findViewById(R.id.crop).findViewById(R.id.icon);

        iconWarehouse.setImageDrawable(getResources().getDrawable(R.drawable.icon_warehouse, null));
        iconGranary.setImageDrawable(getResources().getDrawable(R.drawable.icon_granary, null));

        iconLumber.setImageDrawable(getResources().getDrawable(R.drawable.icon_lumber, null));
        iconClay.setImageDrawable(getResources().getDrawable(R.drawable.icon_clay, null));
        iconIron.setImageDrawable(getResources().getDrawable(R.drawable.icon_iron, null));
        iconCrop.setImageDrawable(getResources().getDrawable(R.drawable.icon_crop, null));
    }


    private void initTimer() {
        mTimer = new Timer();

        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    mVillage.timerUpdate();
                    village.postValue(mVillage);
                } catch (NullPointerException e) {
                    Log.i("debug", "Null pointer @ResourcePopupActivity/timer");
                }
            }
        },0,1000);
    }
}
