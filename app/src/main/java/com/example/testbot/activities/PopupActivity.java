package com.example.testbot.activities;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

public abstract class PopupActivity extends AppCompatActivity {
    public static boolean isRunning = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        isRunning = true;
        initPopupWindow();
        super.onCreate(savedInstanceState);
    }

    void initPopupWindow() {
        // Set popup window background to be transparent and dim
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        getWindow().setDimAmount(0.3f);
    }


    /**
     * Close window if touch outside
     */
    public void clickedOutside(View view) {
        finish();
    }


    /**
     * Do nothing if touch inside
     */
    public void clickedInside(View view) {

    }


    @Override
    public void onDestroy() {
        isRunning = false;
        super.onDestroy();
    }
}

