package com.example.testbot;

/**
 * Holds all public constants
 */
public final class Constants {
//    public static final class SharedPreferences {
//        public static final class Names {
//        }
//
//        public static final class Keys {
//        }
//    }

    public static final class TribeCssClass {
        public static final String ROMANS    = "tribe1_medium";
        public static final String TEUTONS   = "tribe2_medium";
        public static final String GAULS     = "tribe3_medium";
        public static final String NATARS    = "tribe5_medium";
        public static final String EGYPTIANS = "tribe6_medium";
        public static final String HUNS      = "tribe7_medium";
    }


    public static final class RequestCode {
        public static final int BUILDING_QUEUE = 1;
    }


    public static final class ResultCode {
        public static final int OK = 1;
    }


    public static final class FragmentIndex {
        public static final int OUTER_BUILDINGS = 0;
        public static final int INNER_BUILDINGS = 1;
    }


    public enum FragmentFlags {
        INNER_BUILDINGS,
        OUTER_BUILDINGS
    }


    public enum Tribe {
        Gauls,
        Romans,
        Teutons,
        Natars,
        Egyptians,
        Huns
    }


    /**
     * If a resource storage exceed its capacity in this amount of time (in hours), its progressbar
     * will turn red
     */
    public static final int RESOURCES_CAPACITY_WARNING = 2;

    /**
     * After this amount of time without response, the application will prompt to reload, in ms
     */
    public static final long TIMEOUT_LIMIT = 10000;

    public static final int SECONDS_PER_HOUR = 3600;
    public static final int SECONDS_PER_MINUTE = 60;

    public static final String VERBOSE_LOG_TAG = "No bugs (yet)";

    public static final String[] RESOURCE_URLS = {
        "build.php?id=1",
        "build.php?id=2",
        "build.php?id=3",
        "build.php?id=4",
        "build.php?id=5",
        "build.php?id=6",
        "build.php?id=7",
        "build.php?id=8",
        "build.php?id=9",
        "build.php?id=10",
        "build.php?id=11",
        "build.php?id=12",
        "build.php?id=13",
        "build.php?id=14",
        "build.php?id=15",
        "build.php?id=16",
        "build.php?id=17",
        "build.php?id=18"
    };
}
