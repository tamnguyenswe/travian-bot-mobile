package com.example.testbot.views;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.widget.ProgressBar;

import com.example.testbot.R;
import com.example.testbot.assets.Resource;
import com.example.testbot.assets.VillageResources;

/**
 * Handle GUI related resource information
 */
public class ResourcesView {
    ProgressBar mPbLumber;
    ProgressBar mPbClay;
    ProgressBar mPbIron;
    ProgressBar mPbCrop;

    //Animation duration in ms
    private int mDuration = 0;

    public ResourcesView(Activity activity) {
        mPbLumber   = activity.findViewById(R.id.lumber).findViewById(R.id.progressbar);
        mPbClay     = activity.findViewById(R.id.clay).findViewById(R.id.progressbar);
        mPbIron     = activity.findViewById(R.id.iron).findViewById(R.id.progressbar);
        mPbCrop     = activity.findViewById(R.id.crop).findViewById(R.id.progressbar);

        setAnimationDuration(800);
    }

    ResourcesView() {
        //default constructor, never use this
    }

    public void updateResources(VillageResources village) {
        updateProgressBar(mPbLumber, village.lumber);
        updateProgressBar(mPbClay, village.clay);
        updateProgressBar(mPbIron, village.iron);
        updateProgressBar(mPbCrop, village.crop);
    }


    /**
     * Update a resource's progressbar. The progressbar's color depends on how much time left until
     *       it's full.
     * @param progressBar The progressbar to be animated
     * @param resource Resource to display to the progressbar
     */
    @SuppressLint("StaticFieldLeak")
    // Come on, it's just a 300ms animation, how can it cause memory leak?
    private void updateProgressBar(final ProgressBar progressBar, final Resource resource) {
        int valueBefore = progressBar.getProgress();
        long valueAfter = resource.getStorageInPercent();
        final boolean isIncreasing = (valueBefore < valueAfter);

        ObjectAnimator mAnimator = ObjectAnimator.ofInt(progressBar, "progress", resource.getStorageInPercent());
        mAnimator.setDuration(mDuration);

        mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            boolean colorChanged = false;
            int warningThreshold = resource.getStorageWarningPercent();

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                if (!colorChanged) {
                    int progress = (int) animation.getAnimatedValue();

                    // if increasing,
                    if (isIncreasing) {
                        // and progress exceeds threshold,
                        if (progress > warningThreshold) {
                            // and not already red, change color to red
                            if (!resource.isWarning) {
                                new AsyncTask<Void, Void, Void>() {
                                    @Override
                                    protected Void doInBackground(Void... voids) {
                                        TransitionDrawable colorTransition = (TransitionDrawable) progressBar.getProgressDrawable();
                                        colorTransition.startTransition(300);
                                        return null;
                                    }
                                }.execute();
                                colorChanged = true;
                                resource.isWarning = true;
                            }
                        }
                    // else if decreasing,
                    } else {
                        // and progress goes below threshold,
                        if (progress < warningThreshold) {
                            // and not green yet, change color to green
                            if (resource.isWarning) {
                                new AsyncTask<Void, Void, Void>() {
                                    @Override
                                    protected Void doInBackground(Void... voids) {
                                        TransitionDrawable colorTransition = (TransitionDrawable) progressBar.getProgressDrawable();
                                        colorTransition.reverseTransition(300);
                                        return null;
                                    }
                                }.execute();
                                colorChanged = true;
                                resource.isWarning = false;
                            }
                        }
                    }

                }
            }
        });
        mAnimator.start();
        progressBar.setProgress(resource.getStorageInPercent());
    }


    public void setAnimationDuration(int duration) {
        mDuration = duration;
    }
}
