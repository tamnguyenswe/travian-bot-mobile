package com.example.testbot.views;

import android.content.res.Resources;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.example.testbot.R;

import java.util.Objects;

/**
 * Handle all pure View-related actions without logic any businesses
 */
public class MainActivityView {


    /**
     * Initialize the one and only toolbar (for now), which contains resources information
     */
    public static void initToolbar(AppCompatActivity activity) {
        Toolbar toolbar = activity.findViewById(R.id.toolbar);
        activity.setSupportActionBar(toolbar);
        Objects.requireNonNull(activity.getSupportActionBar()).setDisplayShowTitleEnabled(false);
    }


    /**
     * Set icons to imageviews in this activity. Can't do with xml code since couldn't get <include>
     * tags to work with parameters
     */
    public static void initIcons(AppCompatActivity activity) {
        ImageView iconLumber = activity.findViewById(R.id.lumber).findViewById(R.id.icon);
        ImageView iconClay = activity.findViewById(R.id.clay).findViewById(R.id.icon);
        ImageView iconIron = activity.findViewById(R.id.iron).findViewById(R.id.icon);
        ImageView iconCrop = activity.findViewById(R.id.crop).findViewById(R.id.icon);

        Resources resources = activity.getResources();

        iconLumber.setImageDrawable(resources.getDrawable(R.drawable.icon_lumber, null));
        iconClay.setImageDrawable(resources.getDrawable(R.drawable.icon_clay, null));
        iconIron.setImageDrawable(resources.getDrawable(R.drawable.icon_iron, null));
        iconCrop.setImageDrawable(resources.getDrawable(R.drawable.icon_crop, null));
    }


    /**
     * Show a snackbar to ask if the user want to hard reload
     */
    public static void showTimedOutSnackbar(Snackbar snackbar) {
        //Disable swipe to dismiss this snack bar
        class NoSwipeBehavior extends BaseTransientBottomBar.Behavior {
            @Override
            public boolean canSwipeDismissView(View child) {
                return false;
            }
        }
        snackbar.setBehavior(new NoSwipeBehavior());

        snackbar.show();
    }
}
