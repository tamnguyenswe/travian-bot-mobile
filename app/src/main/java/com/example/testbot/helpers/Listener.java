package com.example.testbot.helpers;

import android.view.View;

public interface Listener {
    void onButtonClicked(View v, int position);
    void onItemClicked(View v, int position);
}