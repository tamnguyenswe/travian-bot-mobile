package com.example.testbot.helpers;

import android.annotation.SuppressLint;

import static com.example.testbot.Constants.SECONDS_PER_HOUR;
import static com.example.testbot.Constants.SECONDS_PER_MINUTE;

public class Converter {

    /**
     * Convert second to h:mm:ss format
     * @param second Time in second
     * @return Time in h:mm:ss format
     */
    @SuppressLint("DefaultLocale")
    public static String secToString(int second) {

        int hour = second / SECONDS_PER_HOUR;
        second -= hour * SECONDS_PER_HOUR;
        int minute = second / SECONDS_PER_MINUTE;
        second -= minute * SECONDS_PER_MINUTE;

        return String.format("%d:%02d:%02d", hour, minute, second);
    }
}
