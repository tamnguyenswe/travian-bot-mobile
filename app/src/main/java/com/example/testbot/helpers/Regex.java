package com.example.testbot.helpers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex {
    /*
    Mimic python's re library straightforward syntax. Still using java's regular expression pattern syntax though
     */
    public static String findFirst(String regexPattern, String source) {
        /*
            :param regexPattern: String contains java regex pattern
            :param source: source String to match regex pattern to.

            :return: first matched String, or null if none matched
         */
        Matcher matches = Pattern.compile(regexPattern).matcher(source);

        while (matches.find()) {
            return matches.group(1);
        }

        return null;
    }
}
