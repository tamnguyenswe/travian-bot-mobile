package com.example.testbot.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.testbot.Constants.FragmentFlags.OUTER_BUILDINGS
import com.example.testbot.R
import com.example.testbot.adapters.BuildingsRecyclerViewAdapter
import com.example.testbot.assets.Building
import com.example.testbot.helpers.Listener

/**
 * A fragment representing a list of outer [Building]s.
 * Activities containing this fragment MUST implement the
 * [BuildingsFragment.OnFragmentInteractionListener] interface.
 */
class OuterBuildingsFragment : BuildingsFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_outer_buildings, container, false)

        with(view as RecyclerView) {
            layoutManager = LinearLayoutManager(context)

            adapter = BuildingsRecyclerViewAdapter(activity, object : Listener {
                override fun onItemClicked(v: View?, position: Int) {
                    // TODO: Not implemented
                }

                override fun onButtonClicked(v: View?, position: Int) {
                    mListener!!.onButtonClicked(position, OUTER_BUILDINGS)
                }

            })
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        }
        return view
    }
}
