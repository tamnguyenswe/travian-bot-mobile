package com.example.testbot.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.testbot.R;
import com.example.testbot.assets.Building;
import com.example.testbot.assets.BuildingQueue;
import com.example.testbot.helpers.Listener;

public class QueuingsRecyclerviewAdapter extends RecyclerView.Adapter<QueuingsRecyclerviewAdapter.ViewHolder> {

    private Context mContext;
    private Listener mListener;
    public BuildingQueue buildings;
    public boolean itemMoved;


    public QueuingsRecyclerviewAdapter(Context context, BuildingQueue buildings, Listener listener) {
        this.mContext = context;
        this.buildings = buildings;
        this.mListener = listener;
        this.itemMoved = false;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.layout_queuing_building, parent, false);

        return new QueuingsRecyclerviewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int index) {
        Building building = buildings.get(index);

        viewHolder.buildingName.setText(building.name);
        viewHolder.buildingLevel.setText(String.valueOf(building.level));
        viewHolder.image.setImageResource(building.getImgSource(mContext));
    }


    @Override
    public int getItemCount() {
        return buildings.size();
    }


    public void updateQueue(BuildingQueue buildings) {
        this.buildings = buildings;
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView buildingName;
        TextView buildingLevel;
        ImageButton btCancel;

        RelativeLayout backgroundLayout;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.iv_building_img);
            btCancel = itemView.findViewById(R.id.bt_cancel);
            buildingName = itemView.findViewById(R.id.tv_building_name);
            buildingLevel = itemView.findViewById(R.id.tv_building_level);
            backgroundLayout = itemView.findViewById(R.id.layout_background);

            btCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onButtonClicked(v, getAdapterPosition());
                }
            });

            backgroundLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClicked(v, getAdapterPosition());
                }
            });
        }
    }
}
