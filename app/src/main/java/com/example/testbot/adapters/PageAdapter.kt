package com.example.testbot.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.testbot.fragments.InnerBuildingsFragment
import com.example.testbot.fragments.OuterBuildingsFragment

class PageAdapter(fm: FragmentManager, behavior: Int) : FragmentPagerAdapter(fm, behavior) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0)
            OuterBuildingsFragment()
        else
            InnerBuildingsFragment()
    }

    override fun getCount(): Int {
        return 2 //Number of tabs
    }
}