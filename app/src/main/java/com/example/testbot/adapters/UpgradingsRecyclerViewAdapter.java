package com.example.testbot.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.testbot.R;
import com.example.testbot.assets.UpgradingBuilding;
import com.example.testbot.helpers.Converter;

import java.util.ArrayList;

public class UpgradingsRecyclerViewAdapter extends RecyclerView.Adapter<UpgradingsRecyclerViewAdapter.ViewHolder> {
    private ArrayList<UpgradingBuilding> mBuildings;
    private Context mContext;

    public UpgradingsRecyclerViewAdapter(Context context, ArrayList<UpgradingBuilding> buildings) {
        mBuildings = buildings;
        mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int index) {
        View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_upgrading_building, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int index) {
        UpgradingBuilding building = mBuildings.get(index);

        viewHolder.buildingName.setText(building.name);
        viewHolder.buildingLevel.setText(String.valueOf(building.level));
        viewHolder.image.setImageResource(building.getImgSource(mContext));
        viewHolder.timeLeft.setText(Converter.secToString(building.timeLeft));
    }

    @Override
    public int getItemCount() {
        return mBuildings.size();
    }

    public void updateUpgradingList(ArrayList<UpgradingBuilding> buildings) {
        this.mBuildings = buildings;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView timeLeft;
        TextView buildingName;
        TextView buildingLevel;

        ViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.iv_building_img);
            timeLeft = itemView.findViewById(R.id.tv_time_left);
            buildingName = itemView.findViewById(R.id.tv_building_name);
            buildingLevel = itemView.findViewById(R.id.tv_building_level);
        }
    }
}
