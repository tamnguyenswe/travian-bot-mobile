package com.example.testbot.assets;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

import static com.example.testbot.Constants.RESOURCES_CAPACITY_WARNING;
import static com.example.testbot.Constants.SECONDS_PER_HOUR;

/**
 * Holds a resources' attributes and useful methods.
 * 4 types of resources will be in this class (lumber, clay, iron, crop)
 */
public class Resource implements Parcelable {
    public long capacity;
    public long productionPerHour;
    private double storage;
    private int timeLeft;
    public boolean isWarning = false;

    Resource() {
        capacity = 0;
        storage = 0;
        productionPerHour = 0;
        timeLeft = 0;
    }


    private Resource(Parcel in) {
        capacity = in.readLong();
        storage = in.readDouble();
        productionPerHour = in.readLong();
        timeLeft = in.readInt();
    }

    /**
     *
     * @return This resource's current storage
     */
    public double getStorage() {
        return storage;
    }


    /**
     * Set this resource current storage, and recalculate it's time left
     * @param storage current storage of this resource
     */
    public void setStorage(double storage) {
        this.storage = storage;
        timeLeft = getTimeLeftUntilFull();
    }


    /**
     * Get how much time left until this resource exceeds its capacity, in seconds
     * @return time left until this resource exceeds its capacity, in seconds
     */
    public int getTimeLeftSecond() {
        return timeLeft;
    }


//    /**
//     * Get how much time left until this resource exceeds its capacity, in hour
//     */
//    public double getTimeLeftHour() {
//        return timeLeft * SECOND_PER_HOUR;
//    }


//    public int getWaitTime(long required) {
//        long storageLeft = (long) (this.storage - required);
//
//    }


    /**
     *
     * @return This type of resource's threshold, with which the resource bar can change its color
     */
    public int getStorageWarningPercent() {
        double maximumTime = (double) capacity / productionPerHour;

        return (int) ((maximumTime - RESOURCES_CAPACITY_WARNING) * 100 / maximumTime);
    }


    /**
     *
     * @return current storage/capacity in percent
     */
    public int getStorageInPercent() {
        return (int) (double) (storage * 100 / capacity);
//        return Integer.parseInt(String.valueOf((long) storage * 100 / capacity));
    }


    /**
     *
     * @return time left until this resources type storage exceeds its capacity, in second
     */
    private int getTimeLeftUntilFull() {
        double storageLeft = capacity - storage;
        double timeLeftHour = storageLeft / productionPerHour;
        return (int) (timeLeftHour * SECONDS_PER_HOUR);
    }


    /**
     * Debug function, show current storage, production and capacity
     * @return String contains resource production, storage and capacity
     */
    @SuppressLint("DefaultLocale")
    @Override
    public String toString() {
        return String.format("Storage: %.0f\nProduction: %d\nCapacity: %d", storage, productionPerHour, capacity);
    }


    /**
     * Designed to increase storage each second. Use with care since it can potentially cause bugs
     */
    public void timerUpdateStorage() {
        // only increase if has enough space
        if (storage < capacity)
            storage += (double) productionPerHour / SECONDS_PER_HOUR;
//        Log.i("debug", String.format("Unit/h: %d, Unit/s: %.3f", productionPerHour, (double) (productionPerHour / SECOND_PER_HOUR)));

        if (storage > capacity)
            storage = capacity;


        if (timeLeft > 0)
            timeLeft--;

    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(capacity);
        dest.writeDouble(storage);
        dest.writeLong(productionPerHour);
        dest.writeInt(timeLeft);
    }


    public static final Parcelable.Creator<Resource> CREATOR = new Parcelable.Creator<Resource>() {
        @Override
        public Resource createFromParcel(Parcel in) {
            return new Resource(in);
        }

        @Override
        public Resource[] newArray(int size) {
            return new Resource[size];
        }
    };
}