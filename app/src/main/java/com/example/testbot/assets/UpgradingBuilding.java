package com.example.testbot.assets;

import android.annotation.SuppressLint;
import android.os.Parcel;

public class UpgradingBuilding extends Building {
    public int timeLeft;
    public UpgradingBuilding(String name, int level, int timeLeft) {
        this.name = name;
        this.level = level;
        this.timeLeft = timeLeft;
    }

    private UpgradingBuilding(Parcel in) {
        this.name = in.readString();
        this.level = in.readInt();
        this.timeLeft = in.readInt();
    }

    public static final Creator<Building> CREATOR = new Creator<Building>() {
        @Override
        public UpgradingBuilding createFromParcel(Parcel in) {
            return new UpgradingBuilding(in);
        }

        @Override
        public UpgradingBuilding[] newArray(int size) {
            return new UpgradingBuilding[size];
        }
    };

    @SuppressLint("DefaultLocale")
    @Override
    public String toString() {
        return String.format("%s Level %s, Time Left: %s", name, level, timeLeft);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(level);
        dest.writeInt(timeLeft);
    }
}
