package com.example.testbot.assets;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Stack;

import static com.example.testbot.assets.Building.ComparisonFlags.LEVEL_INSENSITIVE;
import static com.example.testbot.assets.Building.ComparisonFlags.LEVEL_SENSITIVE;

public class BuildingQueue extends LinkedList<Building> implements Parcelable {

    public BuildingQueue() {

    }


    private BuildingQueue(Parcel in) {
        in.readList(this, BuildingQueue.class.getClassLoader());
    }


    /**
     * Add a new Building to the queue. Use this instead of {@link LinkedList#add(Object)} to make
     * the building level show correctly.
     *
     * @param buildingToAdd The Building to add to queue. Should be a {@link Building#clone()} to
     *                      make a deep copy. Shallow copies will cause bug since they would change
     *                      their originals' attributes
     */
    public void addBuilding(Building buildingToAdd) {
        int highestLevelInQueue = buildingToAdd.level;

        for (Building building : this) {
            if (building.getUrl().equals(buildingToAdd.getUrl())) {
                highestLevelInQueue = Math.max(building.level, highestLevelInQueue);
            }
        }

        buildingToAdd.level = highestLevelInQueue + 1;

        super.add(buildingToAdd);
    }


    /**
     * <b>TL;DR</b>: Smoothly remove a Building from the queue. Use this instead of {@link LinkedList#remove()}
     * <p>
     * Long description: Remove targeted Building and all of its higher level references, for example:
     * <p>
     * The queue contains Cranny level 1, 2, 3, 4, the user deletes "Cranny level 3", then not only
     * level 3 but level 4 will also be removed from the queue (all > level 3 references)
     *
     * @param index Index of the building to be removed
     * @return An ArrayList contains all removed indexes
     */
    public ArrayList<Integer> removeBuilding(int index) {
        Building targetBuilding = this.get(index).clone();
        Stack<Integer> markedIndexes = new Stack<>();

        for (int i = 0; i < this.size(); i++) {
            Building building = this.get(i);

            //Mark all references that has higher level than the target to remove.
            if (building.getUrl().equals(targetBuilding.getUrl())) {
                if (building.level >= targetBuilding.level) {
                    markedIndexes.push(i);
                }
            }
        }

        ArrayList<Integer> removedIndexes = new ArrayList<>();

        // Remove all marked buildings from queue
        while (!markedIndexes.isEmpty()) {
            int i = markedIndexes.pop();
            this.remove(i);
            removedIndexes.add(i);
        }

        return removedIndexes;
    }


    /**
     * Sort through the queue after swap 2 buildings with each other, make sure the buildings'
     * levels always in order, for example a Main Building Level 3 cannot be above a Level 2 one.
     */
    public void resortAfterSwap() {

        // Bubble sort's inefficiency can be omitted since the queue won't be too long
        for (int i = 0; i < this.size(); i++) {
            for (int j = i; j < this.size(); j++) {
                Building lowerLevelBuilding = this.get(i);
                Building higherLevelBuilding = this.get(j);
                if (lowerLevelBuilding.isTheSameWith(higherLevelBuilding, LEVEL_INSENSITIVE)) {
                    if (lowerLevelBuilding.level > higherLevelBuilding.level) {
                        Collections.swap(this, i, j);
                    }
                }
            }
        }
    }


    public boolean isTheSameWith(BuildingQueue another) {

        boolean haveTheSameSize = this.size() == another.size();

        if (haveTheSameSize) {
            for (int i = 0; i < this.size(); i++) {
                if (!this.get(i).isTheSameWith(another.get(i), LEVEL_SENSITIVE))
                    return false;
            }

            // return true if each item is the same
            return true;
        }

        return false;
    }


    public static final Creator<BuildingQueue> CREATOR = new Creator<BuildingQueue>() {
        @Override
        public BuildingQueue createFromParcel(Parcel in) {
            return new BuildingQueue(in);
        }

        @Override
        public BuildingQueue[] newArray(int size) {
            return new BuildingQueue[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this);
    }
}
