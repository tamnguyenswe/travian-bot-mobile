package com.example.testbot.assets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.testbot.R;

import static com.example.testbot.Constants.RESOURCE_URLS;

public class Building implements Parcelable {

    private String url;
    private Location mLocation;
    public boolean isBuildingSite = false;

    public String name;
    public int level;

    public enum Location {
        INNER,
        OUTER
    }


    public enum ComparisonFlags {
        /**
         * Comparison returns true if 2 buildings have the same url
         */
        LEVEL_INSENSITIVE,

        /**
         * Comparison returns true if 2 buildings have the same url AND level
         */
        LEVEL_SENSITIVE
    }


    private Context mContext;


    /**
     * Initialize a empty building site. Note that isBuildingSite must always be true
     * @param name Name of this building site. Normally just "Building Site", but it will be
     *             different in different languages
     * @param url  Url contains this building site's location
     * @param isBuildingSite This value must always be true, to confirm that this constructor only be
     *                       used for initializing empty building sites
     */
    public Building(String name, String url, boolean isBuildingSite) {
        if (!isBuildingSite) {
            throw new UnsupportedOperationException("Constructor Building(String, boolean) is only " +
                "for empty building site!");
        }

        this.name = name;
        this.url = User.serverUrl + url;
        this.mLocation = Location.INNER;
        this.isBuildingSite = true;
    }


    /**
     * Initialize a normal building. This function will determine if the building is an outer or inner
     * building, based on its url.
     * @param name  This building's name
     * @param level This buildings's current level
     * @param url   This building's url
     */
    public Building(String name, int level, String url) {
        this.name = name;
        this.level = level;
        this.url = User.serverUrl + url;
        this.isBuildingSite = false;

        for (String resourceUrl : RESOURCE_URLS) {
            if (this.url.equals(resourceUrl)) {
                this.mLocation = Location.OUTER;
                return;
            }

            this.mLocation = Location.INNER;
        }
    }


    public Building() {}


    /**
     * Do <b>not</b>change the order of param init in this method. Name must <b>always</b> be the first
     * one to be initialized, so it won't cause null pointer exception.
     * @param in The {@link Parcel} to be unpacked
     */
    private Building(Parcel in) {
        name = in.readString();
        level = in.readInt();
        url = in.readString();
        isBuildingSite = in.readInt() == 1;

        // Get building location
        for (String resourceUrl : RESOURCE_URLS) {
            assert this.url != null;
            if (this.url.equals(resourceUrl)) {
                this.mLocation = Location.OUTER;
                return;
            }

            this.mLocation = Location.INNER;
        }
    }


    /**
     * Find image source in drawable corresponding with the building's name. Support multiple
     * languages
     *
     * @param context the running application
     * @return the image source corresponding to the building's name
     */
    public int getImgSource(Context context) {
        this.mContext = context;

        if (buildingNameIs(R.string.bn_clay_pit))
            return R.drawable.img_clay_pit;

        else if (buildingNameIs(R.string.bn_cropland))
            return R.drawable.img_cropland;

        else if (buildingNameIs(R.string.bn_woodcutter))
            return R.drawable.img_woodcutter;

        else if (buildingNameIs(R.string.bn_iron_mine))
            return R.drawable.img_iron_mine;

        else if (buildingNameIs(R.string.bn_sawmill))
            return R.drawable.img_sawmill;

        else if (buildingNameIs(R.string.bn_iron_foundry))
            return R.drawable.img_iron_foundry;

        else if (buildingNameIs(R.string.bn_bakery))
            return R.drawable.img_bakery;

        else if (buildingNameIs(R.string.bn_brickyard))
            return R.drawable.img_brickyard;

        else if (buildingNameIs(R.string.bn_grain_mill))
            return R.drawable.img_grain_mill;

        else if (buildingNameIs(R.string.bn_warehouse))
            return R.drawable.img_warehouse;

        else if (buildingNameIs(R.string.bn_granary))
            return R.drawable.img_granary;

        else if (buildingNameIs(R.string.bn_main_building))
            return R.drawable.img_main_building;

        else if (buildingNameIs(R.string.bn_marketplace))
            return R.drawable.img_marketplace;

        else if (buildingNameIs(R.string.bn_embassy))
            return R.drawable.img_embassy;

        else if (buildingNameIs(R.string.bn_cranny))
            return R.drawable.img_cranny;

        else if (buildingNameIs(R.string.bn_town_hall))
            return R.drawable.img_town_hall;

        else if (buildingNameIs(R.string.bn_residence))
            return R.drawable.img_residence;

        else if (buildingNameIs(R.string.bn_palace))
            return R.drawable.img_palace;

        else if (buildingNameIs(R.string.bn_treasury))
            return R.drawable.img_treasury;

        else if (buildingNameIs(R.string.bn_trade_office))
            return R.drawable.img_trade_office;

        else if (buildingNameIs(R.string.bn_stonemasons_lodge))
            return R.drawable.img_stonemasons_lodge;

        else if (buildingNameIs(R.string.bn_brewery))
            return R.drawable.img_brewery;

        else if (buildingNameIs(R.string.bn_great_warehouse))
            return R.drawable.img_great_warehouse;

        else if (buildingNameIs(R.string.bn_great_granary))
            return R.drawable.img_great_granary;

        else if (buildingNameIs(R.string.bn_wonder_of_the_world))
            return R.drawable.img_wonder_of_the_world;

        else if (buildingNameIs(R.string.bn_horse_drinking_trough))
            return R.drawable.img_horse_drinking_trough;

        else if (buildingNameIs(R.string.bn_command_center))
            return R.drawable.img_command_center;

        else if (buildingNameIs(R.string.bn_waterworks))
            return R.drawable.img_waterworks;

        else if (buildingNameIs(R.string.bn_hospital))
            return R.drawable.img_hospital;

        else if (buildingNameIs(R.string.bn_smithy))
            return R.drawable.img_smithy;

        else if (buildingNameIs(R.string.bn_rally_point))
            return R.drawable.img_rally_point;

        else if (buildingNameIs(R.string.bn_stable))
            return R.drawable.img_stable;

        else if (buildingNameIs(R.string.bn_academy))
            return R.drawable.img_academy;

        else if (buildingNameIs(R.string.bn_great_stable))
            return R.drawable.img_great_stable;

        else if (buildingNameIs(R.string.bn_earth_wall))
            return R.drawable.img_earth_wall;

        else if (buildingNameIs(R.string.bn_palisade))
            return R.drawable.img_palisade;

        else if (buildingNameIs(R.string.bn_city_wall))
            return R.drawable.img_city_wall;

        else if (buildingNameIs(R.string.bn_stone_wall))
            return R.drawable.img_stone_wall;

        else if (buildingNameIs(R.string.bn_makeshift_wall))
            return R.drawable.img_makeshift_wall;

        else if (buildingNameIs(R.string.bn_trapper))
            return R.drawable.img_trapper;

        else if (buildingNameIs(R.string.bn_tournament_square))
            return R.drawable.img_tournament_square;

        else if (buildingNameIs(R.string.bn_barracks))
            return R.drawable.img_barracks;

        else if (buildingNameIs(R.string.bn_workshop))
            return R.drawable.img_workshop;

        else if (buildingNameIs(R.string.bn_great_barracks))
            return R.drawable.img_great_barracks;

        else if (buildingNameIs(R.string.bn_heros_mansion))
            return R.drawable.img_heros_mansion;

        return -1;
    }


    /**
     * Clone this instance and return a new deep copy (shallow copy like thisBuilding = thatBuilding
     * only copy reference, so if one changes the other will also change) of this instance
     */
    public Building clone() {
        Building clone = new Building();

        clone.name = this.name;
        clone.level = this.level;
        clone.url = this.url;

        try {super.clone();} catch (Exception ignore) {}

        return clone;
    }


    public Location getLocation() {
        return mLocation;
    }


    /**
     * @return This building's url. Note that this url already contains server address.
     */
    public String getUrl() {
        return url;
    }


    /**
     * Check if these 2 buildings are the same
     * @param anotherBuilding The other Building to compare with
     * @param flag A flag in {@link ComparisonFlags}
     *
     * @return 2 buildings are the same or not
     */
    boolean isTheSameWith(Building anotherBuilding, ComparisonFlags flag) {
        //noinspection EnumSwitchStatementWhichMissesCases
        switch (flag) {
            case LEVEL_INSENSITIVE:
                return this.url.equals(anotherBuilding.url);
            default:
                return (this.url.equals(anotherBuilding.url) && (this.level == anotherBuilding.level));
        }
    }


    private boolean buildingNameIs(int drawableName) {
        return this.name.equals(mContext.getResources().getString(drawableName));
    }


    @SuppressLint("DefaultLocale")
    public String toString() {
        return String.format("%s Level %d", name, level);
    }


    @Override
    public int describeContents() {
        return 0;
    }


    public static final Creator<Building> CREATOR = new Creator<Building>() {
        @Override
        public Building createFromParcel(Parcel in) {
            return new Building(in);
        }

        @Override
        public Building[] newArray(int size) {
            return new Building[size];
        }
    };


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // No need to write isBuildingSite, since empty building site will never be added into
        // a building queue, I guess?...
        dest.writeString(name);
        dest.writeInt(level);
        dest.writeString(url);
        dest.writeInt(isBuildingSite? 1 : 0);
    }
}
