package com.example.testbot.assets

// Prepare for multiple villages
class Village(name : String, url : String){
    var queuingBuildings : BuildingQueue = BuildingQueue()
    var buildings : ArrayList<Building> = ArrayList()
    var upgradings : ArrayList<UpgradingBuilding> = ArrayList()
    var resources : VillageResources = VillageResources()

    var name : String = "Not Initialized"
    var url : String = "Not Initialized"

}
