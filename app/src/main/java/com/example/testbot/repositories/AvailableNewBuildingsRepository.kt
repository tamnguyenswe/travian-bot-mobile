package com.example.testbot.repositories

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.testbot.Constants.VERBOSE_LOG_TAG
import com.example.testbot.assets.Building
import com.example.testbot.assets.User
import org.jetbrains.anko.doAsync
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

class AvailableNewBuildingsRepository {

    @get:JvmName("buildingNames")
    val buildingNames : MutableLiveData<ArrayList<String>> = MutableLiveData()

    @get: JvmName("latestPage")
    val latestPage : MutableLiveData<Document> = MutableLiveData()


    /**
     * Request a list of available [Building]s for this empty building site.
     */
    fun getBuildingList(buildingSite : Building){
        val buildings : ArrayList<String> = ArrayList()

        doAsync {
            var doc : Document = Jsoup.connect(buildingSite.url + "&category=1")
                    .cookies(User.cookies)
                    .get()

            scrapeBuildings(doc, buildings)

            doc = Jsoup.connect(buildingSite.url + "&category=2")
                    .cookies(User.cookies)
                    .get()

            scrapeBuildings(doc, buildings)

            doc = Jsoup.connect(buildingSite.url + "&category=3")
                    .cookies(User.cookies)
                    .get()

            scrapeBuildings(doc, buildings)

            latestPage.postValue(doc)
            buildingNames.postValue(buildings)

            Log.i(VERBOSE_LOG_TAG, "Requested 3 pages @AvailableNewBuildingRepo/getBuildingList")
        }
    }


    /**
     * Crawl buildings' names, add them to [buildings] list
     */
    private fun scrapeBuildings(doc : Document, buildings : ArrayList<String>) {

        for (element in doc.getElementById("build").children()) {
            if (element.attr("class") == "buildingWrapper") {
                val buildingName = element.child(1).child(0).attr("title")
                buildings.add(buildingName)
            }
        }
    }
}
