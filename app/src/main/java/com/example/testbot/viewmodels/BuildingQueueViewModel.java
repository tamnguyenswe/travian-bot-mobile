package com.example.testbot.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.annotation.NonNull;

import com.example.testbot.assets.BuildingQueue;
import com.example.testbot.assets.UpgradingBuilding;

import java.util.ArrayList;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;

public class BuildingQueueViewModel extends AndroidViewModel {
    private MutableLiveData<ArrayList<UpgradingBuilding>> mUpgradingBuildings = new MutableLiveData<>();
    private MutableLiveData<BuildingQueue> mBuildingQueue = new MutableLiveData<>();

    private Timer timer = new Timer();


    public BuildingQueueViewModel(@NonNull Application application) {
        super(application);
    }


    public void setUpgradingBuildings(ArrayList<UpgradingBuilding> upgradingBuildings) {
        mUpgradingBuildings.setValue(upgradingBuildings);
    }


    public void setQueuingBuildings(BuildingQueue buildings) {
        mBuildingQueue.setValue(buildings);
    }


    public LiveData<ArrayList<UpgradingBuilding>> upgradingBuildings() {
        return mUpgradingBuildings;
    }


    public LiveData<BuildingQueue> queuingBuildings() {
        return mBuildingQueue;
    }


    public void initTimer() {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    countDownCurrentUpgrading();
                } catch (NullPointerException ignore) {

                }
            }
        }, 0, 1000);
    }


    /**
     * Count down current upgrading buildings timer.
     */
    private void countDownCurrentUpgrading() {
        ArrayList<UpgradingBuilding> upgradingBuildings = mUpgradingBuildings.getValue();
        Stack<Integer> finishedIndex = new Stack<>();

        assert upgradingBuildings != null;

        for (int index = 0; index < upgradingBuildings.size(); index++) {
            UpgradingBuilding building = upgradingBuildings.get(index);
            building.timeLeft--;
            upgradingBuildings.set(index, building);

            // if finished upgrading, mark this building to remove from list later
            if (building.timeLeft <= 0) {
                finishedIndex.add(index);
            }

//            Log.i("debug", building.toString());
        }

        // remove all finished buildings from list
        for (int index : finishedIndex) {
            upgradingBuildings.remove(index);
        }

        mUpgradingBuildings.postValue(upgradingBuildings);
    }
}
